# A First Level Header
## A Second Level Header

### CODE with 2 Tabs
    class Customer extends SORM
    {

        public function isSalaryGT($value)
        {
            if($this->Salary > $value){
                return true;
            }
            return false;
        }

    }

### CODE The structure
    APP
      ├───application
      │   ├───cache
      │   ├───config
      │   ├───controller
      │   ├───error
      │   ├───library
      │   ├───model
      │   ├───upload
      │   └───view
      │       
      └───system
      |    ├───core  
      |    ├───helper
      |    └───library
      |
      └───.htaccess
      └───index.php


### LIST with *,-,+
* List 1
* List 2
* List 3


### LIST ordered

1. me 1
2. me 2
3. me 3


### LINKS

This is a [example](www.google.lk) link

I get 10 times more traffic from [Google][1] than from
[Yahoo][2] or [MSN][3].

[1]: http://google.com/        "Google"
[2]: http://search.yahoo.com/  "Yahoo Search"
[3]: http://search.msn.com/    "MSN Search"


### Paragraph

Now is the time for all **good** men to come to
the aid of their country. This is just a
regular paragraph.

The quick brown fox jumped over the lazy
dog's back.

### blockquote 1

>the aid of their country. This is just a
regular paragraph.

### blockquote 2

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> ## This is an H2 in a blockquote


I strongly recommend against using any `<blink>` tags.

I wish SmartyPants used named entities like `&mdash;`
instead of decimal-encoded entites like `&#8212;`.


4 < 5
